package com.app.weather.model;

/**
 * Author:
 */
public class CityWeatherInfo {

    private String cityCode;
    private String population;
    private String temperature;

    public CityWeatherInfo(){}

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }


    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

}
