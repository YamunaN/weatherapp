package com.app.weather.controller;

import com.app.weather.model.CityWeatherInfo;
import com.app.weather.service.WeatherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Author:
 * */
@RestController
@RequestMapping("/city")
public class WeatherController {

	private final Logger log = LoggerFactory.getLogger(WeatherController.class.getName());
	
	@Autowired
	WeatherService weatherService;


	@RequestMapping(value = "/{code}", method = RequestMethod.GET)
	public ResponseEntity<CityWeatherInfo> getWeatherDetails(@PathVariable String code)
			throws IOException, InterruptedException, ExecutionException {
		log.info("in controller getWeatherDetails Code :" + code);
		CityWeatherInfo city = new CityWeatherInfo();
		CompletableFuture<String> cityPopulation = weatherService.getCityPopulation(code);
		CompletableFuture<String> temp = weatherService.getTemperature(code);
		CompletableFuture completableFuture = CompletableFuture.allOf(cityPopulation, temp).exceptionally(
				exception -> {
					System.out.println("in exceptionally");
					System.err.println(exception); return null;}
		);
		completableFuture.join();
		city.setCityCode(code);
		city.setPopulation(cityPopulation.get());
		city.setTemperature(temp.get());
		return new ResponseEntity(city, HttpStatus.OK);
	}

}
