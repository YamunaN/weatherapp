package com.app.weather.service;

import com.app.weather.model.CityInfo;
import com.app.weather.repository.CityRepository;
import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;


/**
 * Author:
 */
@Service
public class WeatherService {
    private final Logger log = LoggerFactory.getLogger(WeatherService.class.getName());

    @Autowired
    private CityRepository cityRepository;
    @Autowired
    private RestTemplate restTemplate;

    @Async
    public CompletableFuture<String> getCityPopulation(String code) {
        log.info("in getCity");
        CityInfo cityInfo = cityRepository.findByCityCode(code);
        return CompletableFuture.completedFuture(cityInfo.getPopulation());
    }

    @Async
    public CompletableFuture<String> getTemperature(String code) throws IOException {
        log.info("in getTemperature");
        String url = "https://samples.openweathermap.org/data/2.5/weather?q=" + code + "&appid=b6907d289e10d714a6e88b30761fae22";
        try {
            JsonNode root = restTemplate.getForObject(url, JsonNode.class);
            String temp = root.at("/main/temp").asText();
            return CompletableFuture.completedFuture(temp);
        } catch (Exception e) {
            log.error("Parsing failed IOException " + e.getMessage());
            throw new IOException(e.getMessage());
        }
    }

}
