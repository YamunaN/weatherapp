package com.app.weather.repository;

import com.app.weather.model.CityInfo;
import org.springframework.data.repository.CrudRepository;

/**
 * Author:
 */

public interface CityRepository extends CrudRepository<CityInfo, String> {

    CityInfo findByCityCode(String code);
}
