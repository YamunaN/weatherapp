package com.app.weather;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Author:
 * */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=WeatherControllerTest.class)
@WebMvcTest(value=WeatherControllerTest.class)
public class WeatherControllerTest {

	@Autowired
	private MockMvc mockMvc;

	private String cityCode = "PL";

	@Test
	public void getWeatherTest() throws Exception {
		MvcResult result = mockMvc.perform(get("/city/"+cityCode)).andExpect(MockMvcResultMatchers.request().asyncStarted())
				.andReturn();
		mockMvc
				.perform(asyncDispatch(result))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.content[0].cityCode").value("PL"))
				.andExpect(jsonPath("$.content[0].population").value("10000"))
				.andExpect(jsonPath("$.content[0].temperature").value("280.32"));
	}

}
